import { Avatar } from '@material-ui/core'
import React from 'react'
import './VideoCard.css'

const VideoCard = ({image, title, channel, views, timeStamp, channelImage}) => {
    return (
        <div className="videocard">
            <img src={image} className='videocard_image' alt=""/>
            <div className="videocard_info">
                <Avatar
                    className="videocard_avatar"
                    alt={channel}
                    src={channelImage}
                />
                <div className="videocard_text">
                    <h4>{title}</h4>
                    <p>{channel}</p>
                    <p>{views} views &#9679; {timeStamp} </p>
                </div>
            </div>
        </div>
    )
}

export default VideoCard
