import React, { useEffect, useState } from "react";
import "./Recommended.css";
import axios from "axios";
import { DateTime } from "luxon";
import Alert from "@material-ui/lab/Alert";
import { CircularProgress } from "@material-ui/core";
import VideoCard from "../VideoCard/VideoCard";

const api_key = "AIzaSyBbyr-Eb3uTU6Qg_Db634aY7HuLXJ8c3o0";
let i = 0;
let j = 1;
const Recommended = () => {
  const [videoCards, setVideoCards] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [isBottom, setIsBottom] = useState(false);
  const [nextPageToken, setNextPageToken] = useState("");

  useEffect(() => {
    axios
      .get(
        `https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&chart=mostPopular&maxResults=25&regionCode=US&key=${api_key}`
      )
      .then((response) => {
        console.table(response.data);
        setNextPageToken(response.data.nextPageToken);
        createVideoCards(response.data.items);
      })
      .catch((error) => {
        console.log(error);
        setIsError(true);
      });
  }, []);

  useEffect(() => {
    if (isBottom) {
      addItems();
    }
  }, [isBottom]);

  function handleScroll(e) {
    if (e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight) {
      console.log("true");
      setIsBottom(true);
    }
  }

  const addItems = () => {
    console.log("loading...");
    i++;
    let endpt = "";
    if (i * 25 > 50 * j) {
      j++;

      endpt = `https://www.googleapis.com/youtube/v3/videos?pageToken=${nextPageToken}&part=snippet%2CcontentDetails%2Cstatistics&chart=mostPopular&maxResults=${
        25 * (i % 2)
      }&regionCode=US&key=${api_key}`;
    } else {
      endpt = `https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&chart=mostPopular&maxResults=${
        25 * i
      }&regionCode=US&key=${api_key}`;
    }
    axios
      .get(endpt)
      .then((response) => {
        console.table(response.data.items);
        createVideoCards(response.data.items);
      })
      .catch((error) => {
        console.log(error);
        setIsError(true);
      });
    setIsLoading(true);
    setIsBottom(false);
  };

  async function createVideoCards(videoItems) {
    let newVideoCards = [];
    for (const video of videoItems) {
      const videoId = video.id;
      const snippet = video.snippet;
      const channelId = snippet.channelId;

      const response = await axios.get(
        `https://www.googleapis.com/youtube/v3/channels?part=snippet&id=${channelId}&key=${api_key}`
      );

      const channelImage = response.data.items[0].snippet.thumbnails.medium.url;
      const title = snippet.title;
      const image = snippet.thumbnails.medium.url;
      const views = video.statistics.viewCount;
      const timeStamp = DateTime.fromISO(snippet.publishedAt).toRelative();
      const channel = snippet.channelTitle;

      newVideoCards.push({
        videoId,
        image,
        title,
        channel,
        views,
        timeStamp,
        channelImage,
      });
    }
    setVideoCards(newVideoCards);
    setIsLoading(false);
  }

  if (isError) {
    return (
      <Alert severity="error" className="loading">
        No Results found!
      </Alert>
    );
  }

  return (
    <section className="recommended" onScroll={handleScroll}>
      <div className="recommended_videos">
        {videoCards.map((item) => {
          return (
            <VideoCard
              key={item.videoId}
              title={item.title}
              image={item.image}
              views={item.views}
              timeStamp={item.timeStamp}
              channel={item.channel}
              channelImage={item.channelImage}
            />
          );
        })}
      </div>
      {isLoading ? (
        <div style={{ textAlign: "center" }}>
          <CircularProgress className="loading" color="secondary" />
        </div>
      ) : null}
    </section>
  );
};

export default Recommended;
