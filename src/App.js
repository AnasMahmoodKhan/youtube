import React from 'react';
import './App.css';
import Header from './components/Headers/Header';
import Sidebar from './components/Sidebar/Sidebar';
import Recommended from './components/Recommended/Recommended';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {

  return (
    <div className="App">
      <Router>
      <Header />
        <Switch>
          <Route path='/'>
            <section className="app__mainpage">
              <Sidebar />
              <Recommended />
            </section>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;